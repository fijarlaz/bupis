<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/gallery', 'PagesController@gallery');
Route::get('/about', 'PagesController@about');

Route::resource('status', 'StatusController');


Route::get('/login', 'MainController@login');
Route::get('/logout', 'MainController@logout');
Route::get('/secret', 'MainController@secret');
