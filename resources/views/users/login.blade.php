@extends('layouts.base')


@section('header')
    @include('includes.diffheader')

@endsection
@section('content')
    {{-- {!! Form::open() !!}
    <div class="form-group">
            {{Form::label('name', 'Name:')}}
            {{Form::text('Name','', ['class' => 'form-control'])}}

            {{Form::label('password', 'Password: ')}}
            {{Form::password('password', '', ['class' => 'form-control'])}}

    </div>
    {{Form::submit('Log in', [
        'class' => 'btn btn-primary'
        ])}}
    {!! Form::close() !!} --}}

{{-- form login --}}
    <div class="form-group login-form" style="padding-top:5rem;">
        {!! Form::open(['class' => 'text-center border border-light p-5']) !!}
        <p class="h4 mb-4">Sign in</p>
            {{Form::label('username', 'Username:')}}
            {{Form::text('username','',['class'=>'form-control mb-4'])}}
            <br><br>
            {{Form::label('password', 'Password:')}}
            {{Form::password('password',['class'=>'form-control mb-4'])}}
            <div class="d-flex justify-content-around">
        <div>
            <!-- Remember me -->
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
            </div>
        </div>
        <div>
            <!-- Forgot password -->
            <a href="">Forgot password?</a>
        </div>
    </div>
    <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

    <!-- Register -->
    <p>Not a member?
        <a href="">Register</a>
    </p>

    <!-- Social login -->
    <p>or sign in with:</p>
    <a class="btn btn-social-icon btn-twitter">
    <span class="fa fa-twitter"></span>
  </a>
    {{-- <a class="btn-floating btn-lg btn-fb" type="button" role="button"><i class="fab fa-facebook-f"></i></a>
    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-twitter"></i>
    </a>
    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-linkedin-in"></i>
    </a>
    <a type="button" class="light-blue-text mx-2">
        <i class="fab fa-github"></i>
    </a> --}}

        {!! Form::close() !!}
    </div>

    {{--  --}}
    <div class="" style="text-align:center; padding-top:2rem;">
        <h4>- - - - - or - - - - -</h4>
        <a href="{{action('MainController@secret')}}" class="btn btn-warning">Log in with SSO (UI)</a><br>
        <button type="button" class="btn btn-gplus btn-danger"><i class="fab fa-google-plus-g pr-1"></i> Google +</button>
    </div>
@endsection
