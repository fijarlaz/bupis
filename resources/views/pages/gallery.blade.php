@extends('layouts.base')

@section('header')
    @include('includes.diffheader')

@endsection

@section('content')
    <div class="text-field" style="text-align:center;">
        <h1>These are our daily activities</h1>
    </div>
<div class="row" id="photos">
</div>
<script type="text/javascript" src="{{asset('js/photos/photos.js')}}">

</script>
@endsection
