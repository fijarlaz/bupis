@extends('layouts.base')


@section('header')
    @include('includes.diffheader')

@endsection
@section('content')
<h2>If i'm not mistaken, this will be an about page</h2>
<br>
@if (Auth::check())
    <p>{{$user->name}}</p>
@endif
<a href="{{action('MainController@logout')}}" class="btn btn-danger">Logout</a>
@endsection
