

@extends('layouts.base')

@section('header')
    <style>
        header {
            height: 100% !important;
        }
    </style>
    @include('includes.header')

 <div class="view intro-2">
   <div class="full-bg-img">
     <div class="mask rgba-black-light flex-center">
       <div class="container text-center white-text">
         <div class="white-text text-center wow fadeInUp">
           <h2>Welcome</h2>
           {{-- <h5>It will always stay visible on the top, even when you scroll down</h5>
           <br>
           <p>Full page intro with background image will be always displayed in full screen mode, regardless
             of device </p> --}}
         </div>
         <a href="{{url('gallery')}}" class="btn">See our activities</a>
         <a href="{{url('about')}}" class="btn">Know more about us</a>

       </div>
     </div>
   </div>
 </div>




@endsection
