<!DOCTYPE html>
<html lang="en" dir="ltr" class="full-height">
    <head>
        @include('includes.head')

        @include('includes.loader')
    </head>
    <body>
        <header>
            @yield('header')
        </header>


        </main>
        <!--Main Layout-->
        <div class="container">
            @yield('content')
        </div>
    <footer>
        @include('includes.footer')
    </footer>

    </body>
</html>
