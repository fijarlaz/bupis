<!-- Footer -->
<footer class="page-footer font-small cyan darken-3" style="background-color:#6b5684 !important;">

    <!-- Footer Elements -->
    <div class="container">

      <!-- Grid row-->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-12 py-5">
          <div class="mb-5 flex-center">
              <h3>Follow us on: <br></h3>


            <!--Instagram-->
            <br>
            <a class="ins-ic" href="http://www.instagram.com/bupislyfe">

              <i class="fab fa-instagram fa-lg white-text mr-md-3 mr-1 fa-2x"></i>
            </a>

          </div>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
    <!-- Footer Elements -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright
      Pondok Bukit Pisang, Jl Hj. Amat, Kukusan, Beji, Depok.
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
