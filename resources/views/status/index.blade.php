@extends('layouts.base')


@section('header')
    @include('includes.diffheader')

@endsection
@section('content')

    <h1>Status</h1>
    @if(count($statuses) > 0)
        @foreach($statuses as $status)
            <div class="well">
                <div class="row">

                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/status/{{$status->id}}">{{$status->status}}</a></h3>
                        <small>Written on {{$status->created_at}} </small>
                    </div>
                </div>
            </div>
        @endforeach
        {{$statuses->links()}}
    @else
        <p>No posts found</p>
    @endif
@endsection
