@extends('layouts.base')


@section('header')
    @include('includes.diffheader')

@endsection
@section('content')
    <a href="/status" class="btn btn-default">Go Back</a>
        <h1>{{$status->name}}</h1>
        <br><br>
        <div>
            {!!$status->status!!}
        </div>
        <hr>
        <small>Written on {{$status->created_at}}</small>
        <hr>
@endsection
