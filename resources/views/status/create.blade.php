@extends('layouts.base')


@section('header')
    @include('includes.diffheader')

@endsection
@section('content')
    {!! Form::open(['action' => 'StatusController@store', 'method'=>'POST']) !!}
    <div class="form-group">

            {{Form::label('name', 'Name:')}}
            {{Form::text('Name','', ['class' => 'form-control'])}}

            {{Form::label('status', 'Status: ')}}
            {{Form::textarea('Status', '', ['class' => 'form-control'])}}

    </div>
    {{Form::submit('Submit', [
        'class' => 'btn btn-primary'
        ])}}
    {!! Form::close() !!}

@endsection
