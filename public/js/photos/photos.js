$(document).ready(function() {
    $.ajax({
        url: 'api/photos',
        type: 'GET',
        dataType: 'JSON',
        success: function(data) {
            // console.log(data);
            var card='';
            for (var i = 0; i < data.data.length; i++) {
                var caption ='';
                // console.log(data.data[i].)
                try {
                    caption+=data.data[i].caption.text;

                } catch (e) {
                    console.log('no caption');
                } finally {

                }
                // caption+=data.data[i].caption.text;
                // console.log(i);
                // caption+= data.data[i].caption.text.toString();
                // console.log(data.data[i].caption.text);
                card+='<div class="col-sm-6 col-lg-4">'; //not closed
                card+='<div class="card">'; //not closed
                card+='<a href="'+data.data[i].link+'" target="_blank">'
                card+='<img class="card-img-top" src="'+data.data[i].images.standard_resolution.url+'" alt="Image">';
                card+='</a>'
                card+='<div class="card-body">'; //not closed
                // card+='<h5 class="card-title">'+data.data[i].caption.text+'</h5>';
                if (caption!=null) {
                    card+='<p class="card-text">'+caption+'</p>';
                    card+='<small>Liked by: '+data.data[i].likes.count + ' people</small>'

                }
                else {
                    card+='<p class="card-text">'+''+'</p>';
                    card+='<small>Liked by: '+data.data[i].likes.count+ ' people</small>'


                }
                card+='</div></div></div>';
            }
            $('#photos').append(card);
        }
    })
    .done(function() {
        console.log("success");
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

});
