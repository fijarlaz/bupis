<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SSO\SSO;
class PagesController extends Controller {
    public function index() {
        return view('pages.welcome');
    }

    public function about() {
        return view('pages.about');
    }
    public function gallery() {
        return view('pages.gallery');
    }


}
